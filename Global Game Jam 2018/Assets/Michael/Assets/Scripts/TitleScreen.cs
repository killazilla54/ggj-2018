﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TitleScreen : MonoBehaviour {


    [SerializeField]
    private Button m_PlayButton;

    [SerializeField]
    private RectTransform m_ScreenWipe;
    [SerializeField]
    private RectTransform m_FlyingBird;

	public AudioClip startGameSound;

    // Use this for initialization
    void Start () {
        m_PlayButton.onClick.AddListener(StartGame);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void StartGame()
    {
		SoundManager.instance.PlaySingle (startGameSound);
        LeanTween.moveLocal(m_FlyingBird.gameObject, new Vector3(6500, m_FlyingBird.transform.localPosition.y), 2.5f);

        LeanTween.size(m_ScreenWipe, new Vector2(3000, m_ScreenWipe.sizeDelta.y), 1f).setOnComplete(() =>
       {
           SceneManager.LoadScene("MAINSCENE");
       }); 
    }
}
