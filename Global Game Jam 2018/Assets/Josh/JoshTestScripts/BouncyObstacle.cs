﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncyObstacle : MonoBehaviour {

    public float bounciness = 0;

	// Use this for initialization
	void Start () {
        //bounciness = this.GetComponent<Collider2D>.bounciness;
	}

    public void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("OCE");
        BouncyPlayerController p = null;
        p = collision.collider.gameObject.GetComponent<BouncyPlayerController>();
        Debug.Log(p);
        if(p)
        {

            p.Bounce(bounciness);
        }
    }

    

    
}
