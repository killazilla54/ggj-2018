﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SloweyObstacle : MonoBehaviour {

    public float slowFactor = 0.5f;

    // Use this for initialization
    public void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("OCE");
        BouncyPlayerController p = null;
        p = collision.gameObject.GetComponent<BouncyPlayerController>();
        Debug.Log(p);
        if (p)
        {

            p.speedMultiplier = slowFactor;
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("OCE");
        BouncyPlayerController p = null;
        p = collision.gameObject.GetComponent<BouncyPlayerController>();
        Debug.Log(p);
        if (p)
        {

            p.speedMultiplier = 1;
        }
    }

    
}
