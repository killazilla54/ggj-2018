﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogSpawnerHandler : MonoBehaviour {

    private float curTime = 0;
    public float timerForChecks = 3;
    public float chanceToSpawn = .2f;
    public float chanceForHeavyFog = .5f;
    public GameObject heavyFogPrefab;
    public GameObject lightFogPrefab;

	// Use this for initialization
	void Start () {
        curTime = timerForChecks;
	}
	
	// Update is called once per frame
	void Update () {
        curTime -= Time.deltaTime;
        if(curTime <= 0.0f)
        {
            float spawnCheck = Random.value;
            if (spawnCheck <= chanceToSpawn)
            {
                Vector3 fogHeight = Vector3.zero;
                if (Random.value <= 0.5) //high or low fog
                {
                    fogHeight = new Vector3(0, 0, -3);
                }
                else
                {
                    fogHeight = new Vector3(0, 0, 3);
                }
                float typeCheck = Random.value;
                if(spawnCheck <= chanceForHeavyFog)
                {
                    GameObject obj = GameObject.Instantiate(heavyFogPrefab, this.transform);
                    obj.transform.position += fogHeight;
                    FogBehavior fog = obj.GetComponent<FogBehavior>();
                    fog.speed = ((Random.value * 2) + 3)*-1;
                }
                else
                {
                    GameObject obj = GameObject.Instantiate(lightFogPrefab, this.transform);
                    obj.transform.position += fogHeight;
                    FogBehavior fog = obj.GetComponent<FogBehavior>();
                    fog.speed = ((Random.value * 2) + 3) * -1;
                }
            }
            curTime = timerForChecks;
        }
        
	}
}
