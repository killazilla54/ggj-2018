﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncyPlayerController : MonoBehaviour {

    public float speed = 0;
    public float accel = 0.1f;
	public float maxSpeed;
    public float speedMultiplier = 1;

	Vector3 mouse_pos ;
	Transform target; //Assign to the object you want to rotate
	Vector3 object_pos;
	float angle;

	public PlayerSprite playerSprite;

	// Use this for initialization
	void Start () {
		target = this.gameObject.transform;
	}

    public void Bounce(float bounciness)
    {
        Debug.Log("Bounce");
        speed = (speed * bounciness) * -1;
    }
	
	// Update is called once per frame
	void Update () {
		mouse_pos = Input.mousePosition;

		object_pos = Camera.main.WorldToScreenPoint (target.position);
		mouse_pos.x = mouse_pos.x - object_pos.x;
		mouse_pos.y = mouse_pos.y - object_pos.y;
		angle = (Mathf.Atan2 (mouse_pos.y, mouse_pos.x) * Mathf.Rad2Deg) - 90;

		playerSprite.UpdateAnimator (angle);

		transform.rotation = Quaternion.Lerp (transform.rotation,Quaternion.Euler (new Vector3 (0, 0, angle)),2*Time.deltaTime);

        speed += accel;
        if (speed >= maxSpeed)
        {
            speed = maxSpeed;
        }

		transform.position += transform.up * Time.deltaTime * speed * speedMultiplier;
	}

    


}
