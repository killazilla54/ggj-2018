﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour {

	public bool hasPackage;
	public Package package;
	public PlayerSprite playerSprite;

	public AudioClip pickupSound;
	public AudioClip dropoffSound;

	public void ReceivePackage(Package package){
		SoundManager.instance.PlaySingle (pickupSound);
		hasPackage = true;
		this.package = package;
		GameManager.instance.transmitterUI.SetTarget (package.destination.position);
	}

	public void DeliverPackage(){
		SoundManager.instance.PlaySingle (dropoffSound);
		hasPackage = false;
        GameManager.instance.UpdateScore(package.value);
		this.package = null;
		GameManager.instance.transmitterUI.ClearTarget ();
	}
}
