﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Package : MonoBehaviour {

	public int value;
	public int baseVal;
	public Town destination;

	public void BuildPackage(Town destination){
		this.destination = destination;
		float dist = Vector3.Distance (transform.position, this.destination.gameObject.transform.position);
		value = Mathf.RoundToInt (baseVal * (dist / 100));
		value = Mathf.Abs (value);
	}
}
