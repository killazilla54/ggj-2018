﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntermissionAnimator : MonoBehaviour {

    private Image img;
    public Sprite[] frames;
    public float frameTime =0.5f;
    private float timer;
    private int curFrameIndex = 0;

	// Use this for initialization
	void Awake () {
        img = this.gameObject.GetComponent<Image>();
        img.enabled = false;
        img.sprite = frames[0];
        curFrameIndex = 0;
        timer = frameTime;
	}
	
	// Update is called once per frame
	void Update () {
        if (img.enabled)
        {
            timer -= Time.deltaTime;
            if (timer <= 0.0f)
            {
                curFrameIndex++;
                if (curFrameIndex >= frames.Length)
                {
                    curFrameIndex = 0;
                }
                img.sprite = frames[curFrameIndex];
                timer = frameTime;
            }
        }
	}

    public void DisableAnim()
    {
        img.enabled = false;

    }

    public void EnableAnim()
    {
        img.enabled = true;
        img.sprite = frames[0];
        curFrameIndex = 0;
        timer = frameTime;
    }
}
