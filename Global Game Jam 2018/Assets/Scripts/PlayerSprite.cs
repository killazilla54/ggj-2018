﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSprite : MonoBehaviour {

	public Transform playerPos;
	Animator anim;


	string state;
	Transform destination;
	float journeyLength;



	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {

		//switch(state){
			//case "flying":
				transform.position = playerPos.position;
		//		break;
		//	case "toTown":
		//	case "fromTown":
		//		float distCovered = 10;
		//		float fracJourney = distCovered / journeyLength;
		//		transform.position = Vector3.Lerp(transform.position, destination.position, fracJourney); //isFracJourney just duration without extra stuff?
		//		break;
		//}

		//Need to have disable switch
		this.transform.position = playerPos.position;

		//float distCovered = (Time.time - startTime) * speed;
		//float fracJourney = distCovered / journeyLength;
		//transform.position = Vector3.Lerp(transform.position, destination.position, fracJourney);
	}

	public void UpdateAnimator(float angle){
		anim.SetFloat ("angle", angle+90);
	}

	public void GetJourneyLengthToTown(Transform destination, bool isDelivering){
		this.destination = destination;
		journeyLength = Vector3.Distance (transform.position, destination.position);
		state = "toTown";
	}

	public void SetState(string state){
		this.state = state;
	}
}
