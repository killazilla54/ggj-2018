﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Town : MonoBehaviour {

    public bool debugMode;
    public MeshRenderer debugRender;
    public Material debugEmpty;
    public Material debugDestination;
    public Material debugHasPackage;

    public Transform position;
    public bool hasWaitingPackage;
    public Package waitingPackage;

	public GameObject packageIcon;

	// Use this for initialization
	void Awake () {
        position = this.gameObject.transform;
        debugRender = this.gameObject.GetComponent<MeshRenderer>();
		packageIcon.SetActive (false);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("PlayerHitsCity");
        PlayerInventory inv = collision.gameObject.GetComponent<PlayerInventory>();
        if(inv != null)
        {
            if (inv.hasPackage && inv.package.destination == this)
            {
                Debug.Log("DeliveringPackage");
				inv.DeliverPackage();

                //Visuals for Delivering Packages for town
                GameManager.instance.ClearTowns();
                GameManager.instance.PopulateTowns(this);
                return;  
            }
            if (!inv.hasPackage && waitingPackage != null)
            {
                Debug.Log("RecievingPackage");
				GameManager.instance.HideAllPackageIcons ();
                inv.ReceivePackage(waitingPackage);
                //waitingPackage.destination.DebugColor(true);
                //Visuals for Recieving Packages for twon
            }
            
        }
    }


    public void ClearTown()
    {
		GameObject.Destroy(waitingPackage);
        waitingPackage = null;
        DebugColor(false);
		packageIcon.SetActive (false);
    }

    public void DebugColor(bool isDestination)
    {
        //Debug.Log("DebugColor");
        if (debugMode && debugRender!=null) {
            if (isDestination)
            {
                debugRender.material.color = Color.green ;
                //Debug.Log("Dest");
                
            }
            else if (waitingPackage != null)
            {
                debugRender.material.color = Color.red;
                //Debug.Log("Package");
            }
            else
            {
                debugRender.material.color = Color.grey;
                //Debug.Log("Empty");
            }
        }
    }
}
