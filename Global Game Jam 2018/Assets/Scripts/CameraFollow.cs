﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

	public Transform target;
    public bool hasSwitchHappened = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float screenQuarter = Camera.main.orthographicSize * Screen.width / Screen.height;
		screenQuarter = screenQuarter / 2;
        if (hasSwitchHappened)
        {
            transform.position = new Vector3(target.position.x - screenQuarter, target.position.y, -100f);
        }
        else
        {
            transform.position = new Vector3(target.position.x + screenQuarter, target.position.y, -100f);
        }
		

	}
}
