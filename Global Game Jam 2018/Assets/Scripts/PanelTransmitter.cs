﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PanelTransmitter : MonoBehaviour {

	private const float MAX_DISTANCE = 550;
	public Transform m_BirdTransform;

    private readonly Color baseColor = new Color(1, 1, 0);

	public Transform m_TargetTransform;

	[SerializeField]
	private Slider m_DistanceSlider;

	[SerializeField]
	private Transform m_ArrowImage;

	[SerializeField]
	private Transform m_PlayerLocation;

	[SerializeField]
	private GameObject m_TownMarkerPrefab;

	[SerializeField]
	private GameObject m_PanelDeliver;

	[SerializeField]
	private GameObject m_PanelSearch;

	[SerializeField]
	private Text m_TextTimer;

	[SerializeField]
	private Text m_TextScore;

	[SerializeField]
	private Text m_BirdPlayerTextTimer;

	[SerializeField]
	private Text m_BirdPlayerTextScore;

    [SerializeField]
    private Image m_CompassBackground;

    [SerializeField]
    private Image m_QuickTimeCircle;

    [SerializeField]
    private Transform m_BirdArrow;

    [SerializeField]
    private Transform m_QuickTimeBurst;

    [SerializeField]
    private GameObject m_BirdFrame;


    [SerializeField]
    private GameObject m_ResultsScreenLeft;
    [SerializeField]
    private GameObject m_ResultsScreenRight;

    [SerializeField]
    private Text m_ResultsScoreLeftText;
    [SerializeField]
    private Text m_ResultsScoreRightText;




    private List<GameObject> m_TownMarkerObjects = new List<GameObject> ();

	private List<Town> m_TownList = new List<Town> ();

	public float MapScale = 550;

    private int m_playerScore = 0;

    //Quicktime Variables
    private float m_QuickTimeSpeed = 1.5f;
    private float m_QuickTimeDelay = 5f;
    private float m_QuickTimeBonusBuffer = 10f;

    private int m_QuickTimeTweenId = -1;

	// Use this for initialization
	void Start () {
		m_DistanceSlider.maxValue = MAX_DISTANCE;

        ClearTarget();
	}

	// Update is called once per frame
	void Update () {
		if (m_BirdTransform != null && m_TargetTransform != null) {
			Vector3 diff = m_BirdTransform.position - m_TargetTransform.position;
			diff.Normalize ();

			float rot_z = Mathf.Atan2 (diff.y, diff.x) * Mathf.Rad2Deg;
			m_ArrowImage.rotation = Quaternion.Euler (0f, 0f, rot_z - 90);

			float dist = Vector3.Distance (m_BirdTransform.position, m_TargetTransform.position);

			m_DistanceSlider.value = dist;
		} else if (m_BirdTransform != null) {

			Rect panelRect = transform.GetComponent<RectTransform> ().rect;
			Vector2 panelSize = new Vector2 (panelRect.width, panelRect.height);

			float xPos = (m_BirdTransform.position.x / MapScale) * panelSize.x;
			float yPos = (m_BirdTransform.position.y / MapScale) * panelSize.y;

			m_PlayerLocation.GetComponent<RectTransform> ().anchoredPosition = new Vector3 (xPos, yPos);
		}

		if (Input.GetKeyDown(KeyCode.RightShift) || Input.GetKeyDown(KeyCode.LeftShift))
        {
            if(m_QuickTimeTweenId > 0)
            {
                LeanTween.cancel(m_QuickTimeTweenId);
                m_QuickTimeTweenId = -1;

                Color celebrateColor = baseColor;
                float diff = Mathf.Abs(m_QuickTimeCircle.transform.localEulerAngles.z - m_ArrowImage.localEulerAngles.z);
                if (diff < m_QuickTimeBonusBuffer)
                {
                    m_QuickTimeCircle.transform.localEulerAngles = m_ArrowImage.localEulerAngles;
                    celebrateColor = Color.green;

                } else if(diff > 100)
                {
                    celebrateColor = Color.red;
                }

                m_QuickTimeCircle.color = celebrateColor;

                if (celebrateColor == Color.green)
                {
                    m_QuickTimeBurst.gameObject.SetActive(true);
                    LeanTween.scale(m_QuickTimeBurst.gameObject, new Vector3(4, 4, 4), 1.5f);
                    LeanTween.alpha(m_QuickTimeBurst.GetComponent<RectTransform>(), 0, .5f).setDelay(.75f);
                }

                LeanTween.value(0, 10, 2).setOnUpdate(value =>
                {
                    float testValue = Mathf.Ceil(value);
                    if ((int)testValue % 2 == 0)
                    {
                        m_CompassBackground.color = baseColor;
                    }
                    else
                    {
                        m_CompassBackground.color = celebrateColor;
                    }
                });

                m_BirdArrow.localEulerAngles = m_QuickTimeCircle.transform.localEulerAngles;
                m_BirdArrow.gameObject.SetActive(true);


                LeanTween.delayedCall(2, () => {
                    m_BirdArrow.gameObject.SetActive(false);
                    m_QuickTimeCircle.gameObject.SetActive(false);
                    LeanTween.delayedCall(m_QuickTimeDelay, StartQuickTimeEvent);
                    m_QuickTimeBurst.GetComponent<Image>().color = Color.green;
                    m_QuickTimeBurst.localScale = Vector3.one;
                    m_QuickTimeBurst.gameObject.SetActive(false);
                    m_CompassBackground.color = baseColor;
                    m_QuickTimeCircle.color = Color.white;
                });
            }
        }
	}

	public void SetTowns (List<Town> townList) {
        print(townList.Count + " towns Set for UI");
        for(int index = 0; index < m_TownMarkerObjects.Count; index++)
        {
            GameObject.Destroy(m_TownMarkerObjects[index]);
        }
        m_TownMarkerObjects.Clear();
		m_TownList = townList;
		foreach (Town town in m_TownList) {
			GameObject newMarker = GameObject.Instantiate (m_TownMarkerPrefab, m_PanelSearch.transform);

			Rect panelRect = transform.GetComponent<RectTransform> ().rect;
			Vector2 panelSize = new Vector2 (panelRect.width, panelRect.height);

			Transform townTransform = town.position;
			float xPos = (townTransform.position.x / MapScale) * panelSize.x;
			float yPos = (townTransform.position.y / MapScale) * panelSize.y;

			newMarker.transform.GetComponent<RectTransform> ().anchoredPosition = new Vector3 (xPos, yPos);

			m_TownMarkerObjects.Add (newMarker);
		}
		m_PanelSearch.SetActive (true);
		m_PanelDeliver.SetActive (false);

        for (int index = 0; index < m_TownList.Count; index++)
        {
            m_TownMarkerObjects[index].SetActive(m_TownList[index].waitingPackage != null);
        }
    }

	public void SetTarget (Transform target) {
		m_TargetTransform = target;
		m_PanelSearch.SetActive (false);
		m_PanelDeliver.SetActive (true);

        LeanTween.delayedCall(m_QuickTimeDelay, StartQuickTimeEvent);

    }

	public void ClearTarget () {
		m_TargetTransform = null;
		m_PanelSearch.SetActive (true);
		m_PanelDeliver.SetActive (false);

		

        LeanTween.cancel(m_QuickTimeTweenId);
        m_QuickTimeTweenId = -1;

        m_BirdArrow.gameObject.SetActive(false);
        m_QuickTimeCircle.gameObject.SetActive(false);
        m_QuickTimeCircle.color = Color.white;
    }

	public void SetTimer (float timeRemaining) {
		float minutes = Mathf.Floor (timeRemaining / 60);
		float seconds = Mathf.Floor (timeRemaining - (60 * minutes));

		m_TextTimer.text = minutes + ":" + seconds.ToString ("00");
		m_BirdPlayerTextTimer.text = minutes + ":" + seconds.ToString ("00");
	}

	public void SetScore (int score) {

        LeanTween.value(m_playerScore, score, .5f).setOnUpdate(value =>
        {
            float floorValue = Mathf.Floor(value);
            m_TextScore.text = floorValue.ToString();
            m_BirdPlayerTextScore.text = floorValue.ToString();
        }).setOnComplete(() =>
        {
            m_playerScore = score;
            m_TextScore.text = score.ToString();
            m_BirdPlayerTextScore.text = score.ToString();
            m_ResultsScoreLeftText.text = score.ToString();
            m_ResultsScoreRightText.text = score.ToString();
        });
		
	}


    //QUICK TIME EVENT
    public void StartQuickTimeEvent()
    {
        if(m_QuickTimeTweenId > 0)
        {
            LeanTween.cancel(m_QuickTimeTweenId);
            m_QuickTimeTweenId = -1;
        }

        m_QuickTimeCircle.gameObject.SetActive(true);
        m_QuickTimeCircle.transform.localEulerAngles = m_ArrowImage.localEulerAngles;

        m_QuickTimeTweenId = LeanTween.rotateAround(m_QuickTimeCircle.gameObject, Vector3.forward, 359, m_QuickTimeSpeed).setLoopClamp().id;
    }

    public void SwapFrames()
    {
        RectTransform currentTransform = transform.GetComponent<RectTransform>();
        RectTransform birdFrameTransform = m_BirdFrame.GetComponent<RectTransform>();
        Vector2 anchorMax = currentTransform.anchorMax;
        Vector2 anchorMin = currentTransform.anchorMin;
        Vector2 pivot = currentTransform.pivot;
        Vector2 anchoredPosition = currentTransform.anchoredPosition;

        currentTransform.anchorMax = birdFrameTransform.anchorMax;
        currentTransform.anchorMin = birdFrameTransform.anchorMin;
        currentTransform.pivot = birdFrameTransform.pivot;
        currentTransform.anchoredPosition = birdFrameTransform.anchoredPosition;

        birdFrameTransform.anchorMax = anchorMax;
        birdFrameTransform.anchorMin = anchorMin;
        birdFrameTransform.pivot = pivot;
        birdFrameTransform.anchoredPosition = anchoredPosition;
    }

    public void GameOver()
    {
        m_ResultsScreenLeft.SetActive(true);
        m_ResultsScreenRight.SetActive(true);
        LeanTween.delayedCall(2, QuitGame);
    }

    public void QuitGame()
    {
        SceneManager.LoadScene ("TITLESCREEN");
    }
}
