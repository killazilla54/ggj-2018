﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public static GameManager instance = null;

	public Transform playerTransform;
	public Transform targetTown;

	public List<Town> towns;

    private readonly int PHASEONE =  1;
    private readonly int INTERMISSION = 2;
    private readonly int PHASETWO = 3;
    public int currentPhase;

    public float gameLength;
    public float intermissionLength = 5;
	public float timer;
	public int score;

	public GameObject packagePrefab;
    public CameraFollow camFollow;
    public IntermissionAnimator intermission;
	public PanelTransmitter transmitterUI;

	void Awake () {
		//Check if instance already exists
		if (instance == null)
			//if not, set instance to this
			instance = this;
		transmitterUI.m_BirdTransform = playerTransform;
	}

	// Use this for initialization
	void Start () {
        currentPhase = PHASEONE;
        camFollow.hasSwitchHappened = false;
        score = 0;
		timer = gameLength;
		PopulateTowns (towns [0]);

	}

	// Update is called once per frame
	void Update () {
        if (currentPhase == PHASEONE)
        {
            timer -= Time.deltaTime;
            transmitterUI.SetTimer(timer);
            if (timer <= 0.0f)
            {
                currentPhase = INTERMISSION;
                timer = intermissionLength;
                BouncyPlayerController p = playerTransform.GetComponent<BouncyPlayerController>();
                p.speedMultiplier = 0;
                intermission.EnableAnim();
            }
            if (Input.GetKey(KeyCode.R))
            {
                SceneManager.LoadScene("TITLESCREEN");
            }
        }
        else if(currentPhase == INTERMISSION)
        {

            
            timer -= Time.deltaTime;
            if (timer<=0.0f)
            {
                //Fadeout and wait goes here if necissary
                transmitterUI.SwapFrames();
                intermission.DisableAnim();
                camFollow.hasSwitchHappened = true;
                currentPhase = PHASETWO;
                timer = gameLength;
                BouncyPlayerController p = playerTransform.GetComponent<BouncyPlayerController>();
                p.speedMultiplier = 1;

            }

            if (Input.GetKey(KeyCode.R))
            {
                SceneManager.LoadScene("TITLESCREEN");
            }
        }
        else if(currentPhase == PHASETWO)
        {

            timer -= Time.deltaTime;
            transmitterUI.SetTimer(timer);
            if (timer <= 0.0f)
            {
                GameOver();
            }

            if (Input.GetKey(KeyCode.R))
            {
                SceneManager.LoadScene("TITLESCREEN");
            }
        }
	}


	//Update Score
	public void UpdateScore (int score) {
		this.score += score;
		transmitterUI.SetScore (this.score);
	}

	private void GameOver () {
        //SceneManager.LoadScene ("TITLESCREEN");
        transmitterUI.GameOver();
    }

	//For assigning to a package
	public Town GetRandomDestTown (Town startingTown) {
        
        Town town = towns [Random.Range (0, towns.Count)];
        while(town == startingTown)
        {
            town = towns[Random.Range(0, towns.Count)];
        }

        return town;

	}


	public void PopulateTowns (Town deliveryTown) {
		//loop through all towns, create new package (package score and destination is in package constructor)
		int numberOfTowns = 4;
		List<int> chosen = new List<int> ();

		// get x number of new towns and populate with package
		// for each
		// generate a random index
		//check if index is in chosen already, if so, re-roll;
		for (int i = 0; i < numberOfTowns; i++) {
			//int newIndex = Random.Range (0, towns.Count);
			bool found = true;
			int newIndex = 0;
			while (found == true) {
				found = false;
				newIndex = Random.Range (0, towns.Count);
				foreach (int index in chosen) {
					if (newIndex == index) {
						found = true;
					}
				}
				//if found is false, add
			}
			print (newIndex);
			chosen.Add (newIndex);
		}

		foreach (int index in chosen) {
			Town town = GetRandomDestTown (towns[index]);
			GameObject pack = Instantiate (packagePrefab);
			pack.GetComponent <Package>().BuildPackage (town);
			print ("Populating: " + index + " - with: " + pack);
			towns [index].waitingPackage = pack.GetComponent <Package>();  //.PopulateTown (); //<--Josh's method will go here

			towns[index].packageIcon.SetActive (true);
            
			towns[index].DebugColor(false);

        }
		//Send updated list to UI;
		transmitterUI.SetTowns (towns);
	}

	public void ClearTowns () {
		//loop through all towns, destroy remaining packages
		foreach (Town town in towns) {
			town.ClearTown ();
		}
	}

	public void HideAllPackageIcons(){
		foreach (Town town in towns) {
			town.packageIcon.SetActive (false);
		}
	}


}
