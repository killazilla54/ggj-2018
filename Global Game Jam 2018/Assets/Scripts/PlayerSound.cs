﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSound : MonoBehaviour {

	public AudioClip wings;

	// Use this for initialization
	void Start () {
		//StartCoroutine ("FlapWings");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator FlapWings(){
		while (true) {
			SoundManager.instance.PlaySingle (wings);

			yield return new WaitForSeconds (1f);
		}
	}
}
